/*! \file strwx.c
 * 
 *  \brief String Word Extract
 *
 *  int   strwx( to, from, start, num )
 *
 *  char *to;	- Pointer to destination string
 *  char *from;	- Pointer to source string
 *  int start;	- First word from left to extract (1=leftmost)
 *  int num;	- Number of words to extract
 *
 *  The specified number of words or less are extracted from the source
 *  to the destination string, and the destination is terminated.
 *
 *  "strwx" operates beginning with the word in the position indicated by
 *  "start" and extracts "num" words.
 *
 *  Returns:	Actual number of characters extracted
 */
#include <stdio.h>
#include <ctype.h>
#include "libcPlus.h"

#define xmax(a,b) ((int)(a)>(int)(b)?(int)(a):(int)(b))
#define xmin(a,b) ((int)(a)<=(int)(b)?(int)(a):(int)(b))
// int strwc( char * );
// ,strwx(),strwxlf(),strwxrt();

/*! strwx() - string word extract general */
/*! The specified number of words or less are extracted from the source
 *  to the destination string, and the destination is terminated.
 * 
 * \param pd char * - pointer to destination string
 * \param ps char * - pointer to source string
 * \param frst int - First word from left to extract (1=leftmost)
 * \param num int - Number of words to extract
 * \return Actual number of characters extracted
 */
int strwx( char *pd, char *ps, int frst, int num )
{
int done,words,skip,totalwds,chs,inword,xferon;

totalwds=strwc(ps);
words = xmax(0,xmin(num,totalwds));	/* actual number of words to do */
if (!words||frst>totalwds) return 0;	/* if nothing to do		*/
skip = frst - 1;			/* skip this many words		*/
chs=done=inword=0;
if (skip) xferon=0;			/* no transfer while skipping	*/
else xferon = 1;

while (*ps) {
	if (isspace(*ps)) {
		if (inword) {
			inword = 0;
			++done;
			if (done>=(skip+words)) xferon=0;	/* off	*/
			}
		else ;
		}
	else	{
		inword = 1;
		if (done==skip) xferon = 1;
		}
	if (xferon) {			/* transfer while switch on	*/
		*pd++ = *ps;
		++chs;
		}
	else ;
	++ps;
	}
*pd = '\0';
return chs;
}

/*! strwxlf() - string word extract left */
int strwxlf( char *pd, char *ps, int num )
{
int wdsinstr,wdstoget;

wdsinstr = strwc(ps);
wdstoget = xmax(0,xmin(num,wdsinstr));
return strwx(pd,ps,1,wdstoget);
}


/*! strwxrt() - string word extract right */
int strwxrt( char *pd,char *ps,int num )
{
int firstword,wdsinstr,wdstoget;

wdsinstr=strwc(ps);
wdstoget=xmax(0,xmin(num,wdsinstr));
firstword=(wdsinstr-wdstoget)+1;
return strwx(pd,ps,firstword,wdstoget);
}
