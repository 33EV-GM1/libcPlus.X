/*! \file  strdlf.c
 *
 *  \brief String delete characters from left
 *
 */
#include "libcPlus.h"

/*! strdlf - Delete characters from the left of the string */
/*! strdlf() - String Delete (characters) Left
 * 
 *  Characters are deleted from the string pointed to by "str".  The
 *  number of characters to delete is the lesser of "num" or the
 *  string length.  Deletion starts at the left end of the string. 
 *  If this is past the end of the string, no action is taken.  The
 *  string is shortened.
 * 
 * \param str char * - String from which to delete characters
 * \param num int - Number of characters to delete
 * \returns Number of characters actually deleted
 */
int strdlf(char *str, int num)
{
  return strd(str, 0, num);
}

