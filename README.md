## libcPlus - extensions to libc

`libcPlus` contains a number of useful functions for string and
date manipulation, generally more extensive than the standard routines
from `libc`.

Included are such functions as determining the day of the week,
whether a year is a leap year, extracting substrings, extracting
individual words from a string, etc.

documentation in `doxygen/latex/refman.pdf`

* **_dayr** - Day of the year
* **daywk** - Perpetual Calendar: Day of Week
* **isleap** - Determine if a year is a leap year
* **isletter** - Test whether a character is a letter or underscore
* **strcenter** - Center a string
* **strci** - Numeric string comma insert
* **strd** - Delete Characters in a String
* **strdlf** - String delete characters from left
* **strdrt** - String delete characters from right
* **strmove** - Move String
* **strrev** - Reverse a string
* **strrtj** - Right justify string
* **strwc** - String Word Count
* **strwx** - String Word Extract
* **strx** - Extract Characters from String
* **strxlf** - Extract characters from left part of string
* **strxrt** - Extract characters from right part of string
* **ultoa** - Convert unsigned long to ASCII string
