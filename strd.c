/*! \file strd.c
 * 
 * \brief  Delete Characters in a String
 *
 *  int   strd( str, start, num )
 *
 *  str		-- String to operate on
 *  start	-- Position to start at (0=leftmost)
 *  num		-- Number of characters to delete
 *
 *  Characters are deleted from the string pointed to by "str".  The
 *  number of characters to delete is the lesser of "num" or the
 *  string length.  Deletion starts at "start".  If this is past the end
 *  of the string, no action is taken.  The string is shortened.
 *
 *  In all cases the string is cleaned up and the original pointer is valid.
 *
 *  Returns:	Number of characters actually deleted.
 */
#include <string.h>
#include "libcPlus.h"

#define xmax(a,b) ((int)(a)>(int)(b)?(int)(a):(int)(b))
#define xmin(a,b) ((int)(a)<=(int)(b)?(int)(a):(int)(b))

/*! strd - delete characters from a string */
/*! strd() - delete characters from a string
 * 
 * \param str char * - String to operate on
 * \param start int - Position to start at (0=leftmost)
 * \param num int - Number of characters to delete
 * \returns Number of characters actually deleted
 */
int strd(char *str, int start, int num)
{
  int del, slen, i, j;
  char *pt, *ps;

  ps = str;
  slen = strlen(ps);
  del = xmax(0, xmin(slen, num));
  if ((del <= 0) || (start >= slen)) return 0;
  i = j = 0;
  while (*ps)
    {
      if (i == start) break;
      ++ps;
      ++i;
    }
  pt = ps;
  while (*ps)
    {
      if (j == del) break;
      ++ps;
      ++j;
    } /* position this pointer to last char to delete.*/
  strmove(pt, ps); /* copy non-deleted part backwards		*/
  return j;
}

