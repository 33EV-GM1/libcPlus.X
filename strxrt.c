/*! \file  strxrt.c
 *
 *  \brief Extract characters from right part of string
 *
 *  strxrt	- Extract characters from right part of string
 *
 *  int strxrt( dest, source, num )
 *
 *  dest	- Destination string pointer
 *  source	- Source string pointer
 *  start	- Position to start (0 is leftmost)
 *  num		- Number chars to extract & copy
 *
 *  Characters are extracted from "source" and copied to "dest".
 *  The number of characters to copy is "num".
 *
 *  The actual number of characters moved is the lesser of
 *  "num" and the source string.  No check is made to assure that the
 *  destination is large enough to hold the characters copied.
 *
 *  Returns:	Actual number chars copied.
 *
 */
#include <string.h>
#include "libcPlus.h"

#define xmax(a,b) ((int)(a)>(int)(b)?(int)(a):(int)(b))
#define xmin(a,b) ((int)(a)<=(int)(b)?(int)(a):(int)(b))

/*! strxrt - Extract characters from right part of string */
/*! strxrt() - Extract characters from right part of string
 *
 *
 *  Characters are extracted from "source" and copied to "dest".
 *  The number of characters to copy is "num".
 *
 *  The actual number of characters moved is the lesser of
 *  "num" and the source string.  No check is made to assure that the
 *  destination is large enough to hold the characters copied.
 *
 * \param pd char * - pointer to destination string
 * \param ps char * - pointer to source string
 * \param num int - number of characters to extract
 * \return Actual number chars copied
 */
int strxrt(char * pd, char *ps, int num)
{
  int slen, start, cpy;

  slen = strlen(ps);
  cpy = xmax(0, xmin(num, slen));
  start = slen - cpy;
  return strx(pd, ps, start, num);
}
