/*! \file strx.c
 * 
 * \brief Extract Characters from String
 * 
 *  int strx(   dest, source, start, num )
 *
 *  dest	- Destination string pointer
 *  source	- Source string pointer
 *  start	- Position to start (0 is leftmost)
 *  num		- Number chars to extract & copy
 *
 *  Characters are extracted from "source" and copied to "dest".
 *  The number of characters to copy is "num".  The first position
 *  (from left to right) to extract is "start" for "strx" only.
 *
 *  The actual number of characters moved is the lesser of
 *  "num" and the source string.  No check is made to assure that the
 *  destination is large enough to hold the characters copied.
 *
 *  Returns:	Actual number chars copied.
 */
#include "libcPlus.h"

#define xmax(a,b) ((int)(a)>(int)(b)?(int)(a):(int)(b))
#define xmin(a,b) ((int)(a)<=(int)(b)?(int)(a):(int)(b))

/*! strx - extract characters from string */
/*! strx() extract characters from a string
 * 
 *  Characters are extracted from "source" and copied to "dest".
 *  The number of characters to copy is "num".  The first position
 *  (from left to right) to extract is "start" for "strx" only.
 *
 *  The actual number of characters moved is the lesser of
 *  "num" and the source string.  No check is made to assure that the
 *  destination is large enough to hold the characters copied.
 *
 * \param pd char * - pointer to destination string
 * \param ps char * - pointer to source string
 * \param start int - character position to start
 * \param num int - number of characters to extract
 * \return Actual number chars copied
 */
int strx( char* pd, char* ps, int start, int num )
{
int i,j;
j=i=0;
while (*ps) {
	if (i==start) break;
	++i;
	++ps;
	}
if (i<start) return 0;
while (*ps) {
	*pd++ = *ps++;
	++j;
	if (j==num) break;
	}
*pd='\0';
return j;
}
