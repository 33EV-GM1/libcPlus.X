/*! \file _dayr.c
 * 
 * \brief Day of the year
 *
 *  _dayr( month, day, year )
 *
 *  Return the day of the year given month, day, year
 *
 *  Arguments:	month =	integer month 1=jan, etc.
 *		day   = integer day of month 0..31
 *		year  = integer year 1980..
 *
 *  Returns:	Integer day of year 1..366
 */
#include <stdio.h>
#include "libcPlus.h"

/*! _dayr - Day of the year */
/*! _dayr() - given month, day and year return day of the year 
 * 
 * \param mo int - Month 1..12
 * \param da int - Day of the month 0..31
 * \param yr int - integer year 1980..
 * \returns int - Day of the year 1..366
 */
int _dayr(int mo, int da, int yr)
{
  int i, day, leap;
  static int day_tab[2][13] ={
    {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
  };
  day = da;
  leap = isleap(yr);
  for (i = 1; i < mo; i++)
    day += day_tab[leap][i];
  return day;
}

