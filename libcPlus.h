/*! \file libcPlus.h
 *
 * \brief Function prototypes for libcPlus.X.a
 *
 * libcPlus.X.a contains a number of useful string and date routines
 * expanding on the functions available in libc.
 *
 */


/*! daywk - returns the day of the week */
int daywk( int, int, int );

/*! _dayr - Day of the year */
int _dayr( int, int, int );

/*! isleap - Determine if a year is a leap year */
int isleap( int );

/*! isletter - Test whether a character is a letter or underscore */
int isletter( char );

/*! strci - String comma insert. */
void strci( char *);

/*! strd - delete characters from a string */
int strd( char *, int, int);

/*! strdlf - Delete characters from the left of the string */
int strdlf( char *, int );

/*! strdrt - String Delete (characters) Right */
int strdrt( char *, int );

/*! strmove - move a string */
int strmove( char *, char *);

/*! strrev - reverse a string */
void strrev( char * );

/*! strrtj - right-justify string */
void strrtj( char *, int );

/*! strwc - count words in a string */
int strwc( char * );

/*! strx - extract characters from string */
int strx( char *, char *, int, int );

/*! strxlf -Extract characters from left part of string */
int strxlf( char *, char *, int );

/*! strxrt - Extract characters from right part of string */
int strxrt(char * , char *, int );

/*! strwx() - string word extract general */
int strwx( char *, char *, int, int );

/*! strwxlf() - string word extract left */
int strwxlf( char *, char *, int );

/*! strwxrt() - string word extract right */
int strwxrt( char *,char *,int );

/*! strcenter - center a string */
void strcenter(char *, int  );

/*! ultoa - unsigned long to string */
char * ultoa( char *, unsigned long, int );
