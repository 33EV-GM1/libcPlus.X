/*! \file  isletter.c
 *
 *  \brief Test whether a character is a letter or underscore
 *
 */


/*! isletter - Test whether a character is a letter or underscore */
/*! isletter() returns TRUE if character is alphabetic or underline
 *
 * \param c char - Character to be tested
 * \return Return TRUE if character is alphabetic or underline else FALSE
 */
int isletter(char c)
{
  return ((c>='a' && c<='z') || (c>='A' && c<='Z') || c=='_');
}
