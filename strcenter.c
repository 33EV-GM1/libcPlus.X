/*! \file  strcenter.c
 *
 *  \brief Center a string
 *
 *  \author jjmcd
 *  \date May 11, 2018, 7:34 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>

/*! strcenter - */

/*!
 *
 */
void strcenter(char *str, int wide )
{
  int i;
  int dist;
  char work[256];

  dist = (wide-strlen(str))/2;
  for (i=0; i<dist; i++)
    work[i]=' ';
  work[i]='\0';
  strcat(work,str);
  for (i=strlen(work); i<wide; i++)
    work[i]=' ';
  work[i]='\0';
  strcpy(str,work);
}
