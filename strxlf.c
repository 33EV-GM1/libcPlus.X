/*! \file  strxlf.c
 *
 *  \brief Extract characters from left part of string
 * 
 *  int strxlf( dest, source, num )
 *
 *  dest	- Destination string pointer
 *  source	- Source string pointer
 *  start	- Position to start (0 is leftmost)
 *  num		- Number chars to extract & copy
 *
 *  Characters are extracted from "source" and copied to "dest".
 *  The number of characters to copy is "num".
 *
 *  The actual number of characters moved is the lesser of
 *  "num" and the source string.  No check is made to assure that the
 *  destination is large enough to hold the characters copied.
 *
 *  Returns:	Actual number chars copied.
 *
 */
#include "libcPlus.h"

/*! strxlf -Extract characters from left part of string */
/*! strxlf() -Extract characters from left part of string
 *
 *  Characters are extracted from "source" and copied to "dest".
 *  The number of characters to copy is "num".
 *
 *  The actual number of characters moved is the lesser of
 *  "num" and the source string.  No check is made to assure that the
 *  destination is large enough to hold the characters copied.
 *
 * \param pd char * - pointer to destination string
 * \param ps char * - pointer to source string
 * \param num int - number of characters to extract
 * \return Actual number chars copied
 */
int strxlf( char *pd, char *ps, int num )
{
    return strx(pd,ps,0,num);
}
