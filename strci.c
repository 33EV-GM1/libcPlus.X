/*! \file  strci.c
 *
 *  \brief String comma insert
 *
 *  \author jjmcd
 *  \date March 5, 2017, 1:43 PM
 */
/* Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "libcPlus.h"

/*! strci - String comma insert. */

/*! strci() assumes the string is a string of digits, to which
 *  commas are desired at the appropriate places. strci() counts
 *  from the right, inserting a comma every three digits.
 *
 *  LIMITATIONS:
 * \li The string must be large enough to handle the additional
 *     commas. No checking is performed.
 * \li Strings longer than 16 digits are not handled
 *
 * \param s char * - the string to be formatted
 * \return none
 */
void strci( char *s )
{
  int d;

  for ( d=4; d<17; d+=4 )
    {
  if ( strlen(s)<d )
    return;
  strmove(&s[strlen(s)-d+1],&s[strlen(s)-d]);
  s[strlen(s)-d]=',';
    }
}
