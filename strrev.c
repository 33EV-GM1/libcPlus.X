/*! \file  strrev.c
 *
 *  \brief Reverse a string
 *
 *  \author jjmcd
 *  \date March 4, 2017, 7:02 PM
 */
/* Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>

/* From K&R reverse p.62 */

/*! strrev - Reverse a string */
/*! strrev() - Reverse a string.
 *  Exchange a string end for end, in-place
 *
 * \param s char * - String to be reversed
 * \returns - none
 */
void strrev(char *s)
{
  int i,j;
  char c;

  for ( i=0, j=strlen(s)-1; i<j; i++, j-- )
    {
      c = s[i];
      s[i] = s[j];
      s[j] = c;
    }

}
