/*! \file strmove.c
 * 
 * \brief Move String
 *
 *  This moves a string in any direction and any distance.
 *
 *  Uses intermediate string buffer on heap.  Move to temp, then to
 *  destination - this makes possible copying backwards or over source
 *  string partially or in whole.
 *
 *  Returns:	Relative Distance string is moved.  Positive means
 *		left-to-right, 0 means none.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "libcPlus.h"

/*! strmove - move a string */

/*! strmove() moves a string in any direction and any distance. 
 * 
 * \param to char * - Target location
 * \param from char * - Source location
 * \returns int Relative Distance string is moved.  Positive means left-to-right, 0 means none
 */
int strmove(char *to, char *from)
{
  char /*pi1,*/ *ps, *pd;
  int dist, n;
  char pi1[256];

  ps = from;
  pd = to;
  n = strlen(from); /* amount of memory needed	*/
  if (n == 0)
    {
      *to = '\0';
      return 0; /* if nothing to copy		*/
    }
//  pi1 = malloc(n);
  strcpy(pi1, from); /* copy source there		*/
  strcpy(to, pi1); /* done, but need return value	*/
//  free(pi1); /* release the temp storage	*/
  dist = 0;
  if (ps == pd)
    return 0;
  else if (ps > pd)
    while (ps-->pd)
      dist--;
  else if (ps < pd)
    while (ps++<pd)
      dist++;
  return dist;
}
