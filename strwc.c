/*! \file strwc.c
 * 
 * \brief String Word Count
 *
 */
#include <stdio.h>
#include <ctype.h>

/*! strwc - count words in a string */
/*! strwc() returns the number of words in the provided string
 * 
 *  The string is scanned from left to right and words are counted.
 *
 *  The number of words is returned.  The function counts any number
 *  of 1 or more contigious characters bounded by whitespace or the
 *  beginning or end of the string as a word.
 * 
 * \param str char * - String to be tested
 * \return Number of words in the string
 */
int strwc( char *str )
{
int iret,inword;
inword=0;
iret  =0;		/* initially no words counted	*/

if (!isspace(*str)) {
	inword = 1;
	iret   = 1;
	}
while (*str) {
	if (isspace(*str)) {
		if (inword) {
			inword = 0;
			++iret;		/* plus one word */
			}
		else ;
		}
	else inword = 1;
	++str;
	}
return iret;
}
