/*! \file  ultoa.c
 *
 *  \brief Convert unsigned long to ASCII string
 *
 *  \author jjmcd
 *  \date March 4, 2017, 7:00 PM
 */
/* Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "libcPlus.h"

/* Based on K&R itoa p.64 */

/*! ultoa - Convert unsigned long to ASCII string */

/*! ultoa() - Convert unsigned long to ASCII string.
 *  Accepts an unsigned long and returns an ASCII string
 *  representing the value.
 *
 * LIMITT|ATIONS:
 * \li The output string must be long enough to handle the result
 * \li Number bases greater than 36 return nonsensical results
 *
 * \param s char * - Buffer to store the result
 * \param n unsigned long - Value to be converted
 * \param base int - Number base for conversion
 * \returns Pointer to the user's buffer
 */
char * ultoa(char *s, unsigned long n, int base )
{
  int i;

  if ( base > 36 )
    {
      strcpy(s,"invalid");
      return s;
    }

  i=0;
  do
    {
      s[i++] = n%base + '0';
      if ( s[i-1]>'9' )
        s[i-1] += 7;
    } while((n /= base) > 0 );
  s[i]='\0';
  strrev(s);
  return s;
}
