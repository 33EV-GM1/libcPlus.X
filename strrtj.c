/*! \file  strrtj.c
 *
 *  \brief Right justify string
 *
 *  \author jjmcd
 *  \date March 5, 2017, 1:11 PM
 */
/* Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>



/*! strrtj - Right justify string */

/*! Accepts a string and right justifies it in a field whose
 *  width has been provided.  If the string is longer than the
 *  requested field it is left unchanged.
 *
 * LIMITATIONS:
 * \li The string must be long enough to contain the result
 *
 * \param s char * - string to be right justified
 * \param w int - width of the desired field
 * \returns none
 */
void strrtj( char *s, int w )
{
  int dx;               /* Distance to move */
  int i;                /* counter */

  dx = w - strlen(s);   /* How far to move */
  if ( dx<1 )           /* String longer than field? */
    return;             /* Do nothing */
  for ( i=strlen(s)-1; i>=0; i-- )
    {
      s[i+dx] = s[i];
    }
  for ( i=0; i<dx; i++ )
    {
      s[i] = ' ';
    }
  s[w] = '\0';          /* Ensure string terminated */
}

