/*! \file  strdrt.c
 *
 *  \brief String delete characters from right
 *
 */
#include <string.h>
#include "libcPlus.h"

/*! strdrt - String Delete (characters) Right */
/*! strdrt() - String Delete (characters) Right
 * 
 *  Characters are deleted from the string pointed to by "str".  The
 *  number of characters to delete is the lesser of "num" or the
 *  string length.  Deletion starts at the right end of the string. 
 *  If this is past the end of the string, no action is taken.  The
 *  string is shortened.
 * 
 * \param str char * - String from which to delete characters
 * \param num int - Number of characters to delete
 * \returns Number of characters actually deleted
 */
int strdrt(char *str, int num)
{
  int start, slen;

  slen = strlen(str);
  start = slen - num;

  if (start <= 0)
    {
      *str = '\0';
      return slen;
    }
  else return strd(str, start, num);
}